#Tarea 5
#Juan Pablo Madrigal Calderón
#A32951
#Se debe implementar un programa que solicite 2 enteros positivos y
#retorne como resultado  de ellos 

.data
msjIni:		.ascii  "Ingresar dos numeros naturales n y m,\n"
		.ascii  "Ambos deben ser positivos, si se ingresa un valor,\n"
		.asciiz  "negativo se interpreta como sin signo \n"
msjInin:	.asciiz  "Ingresar el dividendo n:\n"
msjInim:	.asciiz  "Ingresar el divisor m:\n"

msjCociente:	.asciiz  "El cociente es:\n"
msjResid:	.asciiz  "El residuo es es:\n"
msjNewline:	.asciiz  "\n"
.text
main:
#Solicitar enteros
	#imprimir mensaje inicial
	la $a0, msjIni
	jal print_msj
	#solicitar dividendo
	la $a0, msjInin
	jal print_msj
	jal read_int
	add $s0,$v0,$0 		#dividendo en $s0 
	#Solicitar divisor
	la $a0, msjInim
	jal print_msj
	jal read_int
	add $s1,$v0,$0		#divisor en $s1
	
	add $a0,$s0,$0		#dividendo en $a0 
	add $a1,$s1,$0		#divisor en $a1
	jal divis
	addu $s3,$v0,$0		#cargamos el cociente en $s3
	addu $s4,$v1,$0		#cargamos el residuo real en $s4
	
	la $a0, msjCociente	#imprimir LSW de producto
	jal print_msj
	addu $a0,$s3,$0
	jal print_int_unsig

	la $a0, msjNewline	#Imprimir salto de pagina
	jal print_msj
	la $a0, msjResid	#Imprimir MSW de producto
	jal print_msj
	addu $a0,$s4,$0
	jal print_int_unsig

	#fin de programa
	li $v0, 10
	syscall

#Bloque de Funciones
#función para imprimir mensajes
print_msj:
	li $v0, 4
	syscall
	jr $ra

#funcion para imprimir enteros
print_int:
	li $v0, 1
	syscall
	jr $ra
#funcion para imprimir enteros sin signo
print_int_unsig:
	li $v0, 36
	syscall
	jr $ra
#funcion para leer enteros
read_int:
	li $v0, 5
	syscall
	jr $ra
#Funcion de dividir
#La funcion recibe en:
#$a0 el dividendo
#$a1 el divisor
#EL residuo  se almacena en 64 bits por medio de 2 registros de 
#32 bits a saber LSW en $v0 y MSW en $v1
#
#Algoritmo basado en figura 3.11 de libro de texto del curso:
#Computer Organization and Design: The Hardware/Software Interface,
#Fifth Edition.
#El divisor se mantiene igual en cada iteración
#El residuo ocupa 64 bits en 2 registros:
#MSW en $v1
#LSW en $v0
#Inicialmente el dividendo se almacena en LSW del residuo
#que es $v0 y ceros en MSW de residuo que es $v1
#Paso 1
#Restar MSW de residuo con divisor y guardarlo en MSW de residuo
#Paso 2
#Si MSB de MSW del residuo es 1 el MSW del residuo se recarga 
#con su valor anterior, después se desplaza 1 bit a la derecha
#y LSB de LSW de residuo queda en 0
#Si MSB de MSW del residuo es 0 el MSW del residuo se mantiene con
#su valor actual, después se desplaza 1 bit a la derecha
#y LSB de LSW se hace 1
#la division de elementos de n bits requiere n+1 iteraciones
#Ultimo paso
#cuando n+1 se desplaza a la derecha $v1 que contiene el residuo
#la funcion retorna el cociente en $v0 y el residuo en $v1
divis:
	addi $sp,$sp,-12 #Reservo espacio en la pila
	sw $ra,8($sp)   #Guardo $ra en pila
	sw $a0,4($sp)   #Guardo $a0 en pila
	sw $a1,0($sp)   #Guardo $a1 en pila
	
	addiu $s2,$0,33  #bits n=32, iteraciones n+1
	addu $t0,$0,$0   #contador de iteraciones
	addu $v0,$a0,$0  #Cargar dividendo en LSW($v0) de residuo
	addu $v1,$0,$0   #cargar MSB ($v1) de residuo con ceros
	lui $t1,0x8000
	ori $t1,$t1,0x0000 #$t1=0x8000000 mascara para detectar valor de 
			   #MSB de residuo
	for:
	beq $t0,$s2,end_for
	addu $t2,$v1,$0   #mantener en $t2 el $v1 actual en case de necesitar
			  #reset
	
	#Paso 1 de algoritmo
	subu $v1,$v1,$a1   #Residuo = Residuo - divisor
	
	#Paso 2
	and $t3,$v1,$t1   #and con $t1 para evaluar MSB de MSW de residuo 
	beq $t3,$0,no_reset  #MSB de MSW de residuo ==0 saltar a no_reset
	#MSB de MSW de residuo ==1 implica reset
	addu $v1,$t2,$0   #reset de MSW de residuo
	srl $t5,$v0,31  #Para obtener en t5 el MSB de $v0 desplazado
			#31 campos a la derecha para poder
			#concaternarlo al LSB de $v1 desplazado
	sll $v1,$v1,1	#sll 1 bit MSW de residuo
	sll $v0,$v0,1	#sll 1 bit LSW de residuo
	or $v1,$v1,$t5  # cargo MSB de $v0 a LSB de $v1 en desplazameinto 
			# para cmpletar desplazamiento de ambos registros a 
			#la izquierda
	addiu $t0,$t0,1 #aumeto contador de iteraciones 
	j for

	no_reset:		#Se mantiene residuo (no reset)
	srl $t5,$v0,31  #Para obtener en t5 el MSB de $v0 desplazado
			#31 campos a la derecha para poder
			#concaternarlo al LSB de $v1 desplazado
	sll $v1,$v1,1	#sll 1 bit MSW de residuo
	sll $v0,$v0,1	#sll 1 bit LSW de residuo
	or $v1,$v1,$t5  # cargo MSB de $v0 a LSB de $v1 en desplazameinto 
			# para cmpletar desplazamiento de ambos registros a 
			#la izquierda
	ori $v0,$v0,1	#set LSB de $v0 en 1
	addiu $t0,$t0,1 #aumeto contador de iteraciones 
	j for	

	end_for:
	srl $v1,$v1,1   #último paso es desplazar una vez a la derecha
			#MSW de residuo ($v1) 

	lw $a1,0($sp)   #recupero de la pila $ra
	lw $a0,4($sp)   #recupero de la pila $ra
	lw $ra,8($sp)   #recupero de la pila $ra
	addi $sp,$sp,12  #libero pila
	jr $ra




	

