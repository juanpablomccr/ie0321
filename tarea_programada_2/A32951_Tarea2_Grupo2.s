#Tarea 2
#Juan Pablo Madrigal Calderón
#A32951
#Se debe implementar un programa que solicite 2 enteros positivos n,k  y 
#realizar una tarea basado en cuales ean eso enteros
#retornar 1 si n==k
#retornar 1 si k==0
#retornar C(n-1,k)+C(n-1,k-1) si n>k>0
# en cualquier otro caso debe retornar un mensaje de notificacion 
#y no ejecutar la función.
#El mensaje de error se despliega después de que ambos enteros n y k
#han sido ingreasdos

.data
msjIni:		.ascii  "Ingresar dos numeros naturales n y k,\n"
		.asciiz  "IMPORTANTE: n debe ser mayor que k\n"
msjInin:	.asciiz  "Ingresar entero positivo n:\n"
msjInik:	.asciiz  "Ingresar entero positivo k:\n"
msjkltzero:	.asciiz  "ERROR:k es menor que cero. Pruebe de nuevo\n"
msjnltzero:	.asciiz  "ERROR:n es menor que cero. Pruebe de nuevo\n"
msjnltk:	.asciiz  "ERROR:n es menor que k. Pruebe de nuevo\n"
msjans:		.asciiz  "C(n,k)="
msjnl:		.asciiz "\n"
.text
main:
#Solicitar enteros
	la $a0, msjIni
	jal print_msj

	la $a0, msjInin
	jal print_msj
	jal read_int
	add $s0,$v0,$0

	la $a0, msjInik
	jal print_msj
	jal read_int
	add $s1,$v0,$0
	
	add $a0,$s0,$0
	add $a1,$s1,$0
	jal test_data
	jal C
	add $t1,$v0,$0
	la $a0,msjans
	jal print_msj
	add $a0,$t1,$0
	jal print_int
	la $a0,msjnl
	jal print_msj	
	j main	

	
#Bloque de Funciones
#función para imprimir mensajes
print_msj:
	#addi $sp, $sp, -4	#reservo espacio en la pila
	#sw $v0, 0($sp)		#guardo v0 ya que lo voy a modificar
	li $v0, 4
	syscall
	#lw $v0, 0($sp)
	#addi $sp, $sp, 4	#libero  espacio en la pila
	jr $ra

#funcion para imprimir enteros
print_int:
	#addi $sp, $sp, -4	#reservo espacio en la pila
	#sw $v0, 0($sp)		#guardo v0 ya que lo voy a modificar
	li $v0, 1
	syscall
	#lw $v0, 0($sp)
	#addi $sp, $sp, 4	#libero  espacio en la pila
	jr $ra
#funcion para leer enteros
read_int:
	#addi $sp, $sp, -4	#reservo espacio en la pila
	#sw $v0, 0($sp)		#guardo v0 ya que lo voy a modificar
	li $v0, 5
	syscall
	#add $t0,$v0,$0
	#lw $v0, 0($sp)
	#addi $sp, $sp, 4	#libero  espacio en la pila
	jr $ra
#función para revisar validez de datos ingresados
test_data:
	addi $sp, $sp, -4	#reservo espacio en la pila
	sw $a0, 0($sp)		#guardo a0 ya que lo voy a modificar

	slt $t0,$a0,$0
	bne $t0,$zero, invalidn_ltzero	#$a0 es n
	slt $t0,$a1,$0
	bne $t0,$zero, invalidk_ltzero	#$a1 es k
	slt $t0,$a0,$a1
	bne $t0,$0, invalid_nltk	#n<k

	lw $a0, 0($sp)		#saco $a0 de la pila
	addi $sp, $sp, 4	#libero  espacio en la pila
	jr $ra
	
	invalidn_ltzero:
	la $a0, msjnltzero
	jal print_msj
	j main

	invalidk_ltzero:
	la $a0, msjkltzero
	jal print_msj
	j main
	
	invalid_nltk:
	la $a0, msjnltk
	jal print_msj
	j main

#funcion recursica C
C:
	
	beq $a1,$0, funceq1	#k=0   func=1
	beq $a0,$a1, funceq1	#n-k   func=1
	
	addi $sp, $sp, -16	#reservo espacio en la pila
	sw $a0, 8($sp)		#guardo a0 ya que lo voy a modificar
	sw $a1, 4($sp)		#guardo a0 ya que lo voy a modificar
	sw $ra, 0($sp)		#guardo a0 ya que lo voy a modificar
	#C(n-1,k)
	addi $a0,$a0,-1
	jal C 
	sw $v0,12($sp)		#guardo $v0 de C(n-1,k)  en pila
	#C(n-1,k-1)
	lw $a1,4($sp)	
	lw $a0,8($sp)	
	addi $a0,$a0,-1
	addi $a1,$a1,-1
	jal C
	
	lw $v1,12($sp)		#cargo de pila v0 de C(n-1,k) en $v1 
		
	add $v0,$v0,$v1		#$v0=$v1+$v0 = C(n-1,k)+C(n-1,k-1)
	lw $ra,0($sp)		
	lw $a1,4($sp)	
	lw $a0,8($sp)	
	addi $sp, $sp,16	#libero pila 
	jr $ra
 
	funceq1:
	addi $v0,$zero,1
	jr $ra


	addi $sp,$sp,4		#libero pila de $v0 de C(n-1,k) 



