#Tarea 4
#Juan Pablo Madrigal Calderón
#A32951
#Se debe implementar un programa que solicite 2 enteros positivos y
#retorne como resultado la multiplicación de ellos 

.data
msjIni:		.ascii  "Ingresar dos numeros naturales n y m,\n"
		.ascii  "Ambos deben ser positivos, si se ingresa un valor,\n"
		.asciiz  "negativo se interpreta como sin signo \n"
msjInin:	.asciiz  "Ingresar el multiplicando n:\n"
msjInim:	.asciiz  "Ingresar el multiplicador m:\n"

msjProdlsw:	.asciiz  "La parta baja del producto es:\n"
msjProdmsw:	.asciiz  "La parta alta del producto es:\n"
msjNewline:	.asciiz  "\n"
.text
main:
#Solicitar enteros
	la $a0, msjIni
	jal print_msj

	la $a0, msjInin
	jal print_msj
	jal read_int
	add $s0,$v0,$0 		#multiplicando en $s0  

	la $a0, msjInim
	jal print_msj
	jal read_int
	add $s1,$v0,$0		#multiplicador en $s1
	
	add $a0,$s0,$0		#multiplicando en $a0 
	add $a1,$s1,$0		#multiplicador en $a1
	jal multip
	addu $s3,$v0,$0
	addu $s4,$v1,$0
	
	la $a0, msjProdlsw	#imprimir LSW de producto
	jal print_msj
	addu $a0,$s3,$0
	jal print_int

	la $a0, msjNewline	#Imprimir salto de pagina
	jal print_msj
	la $a0, msjProdmsw	#Imprimir MSW de producto
	jal print_msj
	addu $a0,$s4,$0
	jal print_int_unsig

	#fin de programa
	li $v0, 10
	syscall

#Bloque de Funciones
#función para imprimir mensajes
print_msj:
	li $v0, 4
	syscall
	jr $ra

#funcion para imprimir enteros
print_int:
	li $v0, 1
	syscall
	jr $ra
#funcion para imprimir enteros sin signo
print_int_unsig:
	li $v0, 36
	syscall
	jr $ra
#funcion para leer enteros
read_int:
	li $v0, 5
	syscall
	jr $ra
#Funcion de multiplicar
#La funcion recibe en:
#$a0 el multiplicando
#$a1 el multiplicador
#EL producto se almacena en 64 bits por medio de 2 registros de 
#32 bits a saber LSW en $v0 y MSW en $v1
#
#Algoritmo basado en figura 3.5 de libro de texto del curso:
#Computer Organization and Design: The Hardware/Software Interface,
#Fifth Edition.
#El multiplicando se mantiene igual en cada iteración
#Inicialmente el multiplicador se almacena en LSW del producto
#que es $v0
#Paso 1
#Si LSB de multiplicador es 1 el resultado de la suma de Producto mas
#multiplicando se almacena en MSW de producto o sea $v1, si es 0
#no re realiza ninguna operación
#Paso 2
#En cada iteración el producto es desplazado un bit a la derecha
#Esto causa que el LSB del multiplicador varíe en cada iteración

multip:
	addi $sp,$sp,-12 #Reservo espacio en la pila
	sw $ra,8($sp)   #Guardo $ra en pila
	sw $a0,4($sp)   #Guardo $a0 en pila
	sw $a1,0($sp)   #Guardo $a1 en pila
	
	addiu $s2,$0,32  #bits n=32
	addu $t0,$0,$0   #contador de iteraciones
	addu $v0,$a1,$0  #Cargar multiplicador en LSW($v0) de producto
	addu $v1,$0,$0   #cargar MSB ($v1) de product0 con ceros
	for:
	beq $t0,$s2,end_for
	and $t1,$v0,1   #and con 1 para evaluar LSB de multiplicador
	#Paso 1 de algoritmo
	beq $t1,$0,op2  #LSB de multiplcador==0
	#LSB de multiplicador ==1
	addu $v1,$v1,$a0 #Producto = producto + multiplicador


	op2:		#Paso 2
	#Paso 2.1 de algoritmo
	sll $t1,$v1,31  #Para obtener en t1 el LSB desplazado
			#31 campos a la izquierda para poder
			#concaternarlo al MSB de $v0 desplazado
	#Paso 2.2 de algoritmo
	srl $v1,$v1,1   #desplazo MSW de producto 1 bit a la derecha
	#Paso 2.3 de algoritmo
	srl $v0,$v0,1   #desplazo LSW de producto 1 bit a la derecha
	or $v0,$v0,$t1  #Transfiero LSB de $v1 en MSB de $v0
	addiu $t0,$t0,1 #aumeto contador de iteraciones 
	j for	

	end_for:

	lw $a1,0($sp)   #recupero de la pila $ra
	lw $a0,4($sp)   #recupero de la pila $ra
	lw $ra,8($sp)   #recupero de la pila $ra
	addi $sp,$sp,12  #libero pila
	jr $ra




	

