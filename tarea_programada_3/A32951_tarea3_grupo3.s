#Tarea 3
#Juan Pablo Madrigal Calderón
#A32951
#Se debe implemetar una función prom que:
# 1. Reciba la dirección de #un array A de palabras con valores enteros 
#positivos,el final del array lo marca un entero negativo. Esto quiere decir
#que el primer elemento que contenga como negativo se interpreta como final 
#del array y se procede a calcular el promedio
# 2. Calcular el promedio de los elementos del array
# 3. Determinar si es factible el cálculo
# Caso factible: retornar $v0=promedio(parte entera)
#			  $V1=residuo
# Caso no factible: retornar $v1=-1, $v0=-1
#De acuerdo al retorno de dicha función
#imprimir en pantalla el array  y el promedio o indicar si promedio no se
#puede calcular
#Se están usando numeros con signo y con palabras de 32 bits
# Rango de trabajo [-(2^31),(2^31)-1] = [-2147483648, 2147483648]

.data
array1:		.word 2,4,8,-1
array2:		.word 15638,25698,78954654,56,-600
array3:		.word 2147483600,2147483601,2147483620,-35
msjError:	.asciiz "Array:\nExiste acarreo, No se puede obtener promedio\n"
msjProm:	.asciiz "Promedio de array es:\n"
msjArray:	.asciiz "Elementos de Array:\n"
msjDiv:		.asciiz ","
newLine: 	.asciiz "\n"
.text
main:
	la $a0, array1		
	jal prom		#calcular promedio del array
	#basado en $v0 imprimir error o imprimir array y parte entera por 
	#medio de función print_result
	jal print_result

	
	la $a0, array2
	jal prom
	jal print_result
	

	la $a0, array3
	jal prom
	jal print_result

	#fin del programa
	li $v0, 10
	syscall

	

	
##Sección de funciones
prom:
	#Esta función es non_leaf y además no modifica el valor de 
	#a0 por lo tanto no guardo nada en la pila
	
	addu $v0,$0,$0    #$v0 = acumulado de suma de elementos
	addu $t0,$0,$0	  #$t0=i=0
	while:
	sll $t1,$t0,2	  #ix4
	add $t1,$t1,$a0   #$t1 = dir A[i]
	lw $t2,0($t1)	  #$t2=A[i]	
	slt $t3,$t2,$0	  #Revisar si A[i] cont. elem. negativo
			  #Si $t3==1 -> Elemento negativo. Fin de array
	bne $t3,$0,calc_prom 
	addu $v0,$v0,$t2   #suma sin excepociones de acumulado y elemento A[i]

	#Para determinar acarreo es importante saber si ambos sumandos son del 
	#mismo signo, Aquí ya sabemos que si lo son por lo que existe 
	#posibilidad de acarreo (Overflow)

	xor $t3,$v0,$t2    #Comparamos result. de suma (acumulado actual $v0)
			   # y A[i] 
	slt $t3,$t3,$0	   #t3<0 implica acarreo, o sea $t3==1 -> Overflow
	bne $t3,$0,Overflow
	#No Overflow, se aumenta contador y se empieza nueva iteración
	addiu $t0,$t0,1	   #i++
	j while
	
	calc_prom:
	#ultimo elemento no es parte del calculo del  promedio pero se deja 
	#$t0 con su valor n  ya que tenemos que contar el elemento A[o]; 
	#incluir el elemnto[0] 
	divu $v0,$t0	   # (A[0]+A[1]+...+A[n-1]) / n
	mflo $v0	   # cargar cociente desde $lo a $v0
	mfhi $v1	   # cargar residuo desde $hi a $v1
	jr $ra

	Overflow:
	addiu $v0,$0,-1	
	addiu $v1,$0,-1	
	jr $ra

#Función para imprimir resultados
print_result:		#leaf function, en este caso solo interesa guardar $ra
	addi $sp,$sp,-12 #Reservo espacio en la pila
	sw $ra,8($sp)   #Guardo $ra en pila
	sw $a0,4($sp)   #Guardo $ra en pila
	sw $v0,0($sp)   #Guardo $ra en pila

	slt $t1,$v0,$0  #si $v0==-1 Overflow
	bne $t1,$0,print_err
	la $a0, msjProm
	jal print_msj   #Imprimir mensaje de promedio
	lw $v0,0($sp)	
	lw $a0,4($sp)	
	addu $a0,$v0,$0
	jal print_int	#imprimir el valor del promedio

	la $a0, newLine
	jal print_msj   #Imprimir salto después de promedio
	lw $a0,4($sp)	#recupero de la pila dir de arrayX
	jal print_array #imprimir el array
	
	
	lw $v0,0($sp)   #recupero de la pila $ra
	lw $a0,4($sp)   #recupero de la pila $ra
	lw $ra,8($sp)   #recupero de la pila $ra
	addi $sp,$sp,12  #libero pila
	jr $ra
	
	print_err:
	la $a0, msjError
	jal print_msj   #imprimir mensaje de error de promedio incalculable
	lw $v0,0($sp)   #recupero de la pila $ra
	lw $a0,4($sp)   #recupero de la pila $ra
	lw $ra,8($sp)   #recupero de la pila $ra
	addi $sp,$sp,12  #libero pila
	jr $ra
#función para imprimir mensajes
print_msj:
	li $v0, 4
	syscall
	jr $ra

#funcion para imprimir enteros
print_int:
	li $v0, 1
	syscall
	jr $ra

print_array:		  #leaf_fuction, solo me interesa guardar $ra en pila
	addi $sp,$sp,-12 #Reservo espacio en la pila
	sw $v0,8($sp)   #Guardo $v0 en pila pq lo voy a modificar
	sw $ra,4($sp)   #Guardo $ra en pila
	sw $a0,0($sp)   #Guardo $a0 en pila
	
	la $a0, msjArray
	jal print_msj	
	addu $t0,$0,$0	  #$t0=i=0
	
	lw $a0,0($sp),   #recupero de la pila $v0
	while1:
	sll $t1,$t0,2	  #ix4
	add $t1,$t1,$a0   #$t1 = dir A[i]
	lw $t2,0($t1)	  #$t2=A[i]	
	slt $t3,$t2,$0	  #si $t3==1 -> fin array, no imrimir separador
	bne $t3,$0,end_print 
	add $a0,$0,$t2
	jal print_int	  #imprimir A[i] 
	lw $a0,0($sp)
	lw $t4,4($t1)	  #$t4=A[i+1]	
	slt $t3,$t4,$0	  #si $t3==1 -> fin array, no imrimir separador
	bne $t3,$0,no_print_div
	la $a0, msjDiv
	jal print_msj
	lw $a0,0($sp)
	addi $t0,$t0,1    #i++
	j while1

	no_print_div:
	addi $t0,$t0,1
	j while1	
	
	end_print:
	la $a0,newLine
	jal print_msj
	lw $a0,0($sp)
	lw $ra,4($sp)   #recupero de la pila $ra
	lw $v0,8($sp)
	addi $sp,$sp,12  #libero pila
	jr $ra

		




