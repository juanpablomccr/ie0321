#Tarea 2
#Juan Pablo Madrigal Calderón
#A32951
#Se debe implementar un menu con espera de entrada perpetuo, es decir
#siempre listo para recibir un comando.
#La estrategia es partir de un case de c 
#case:
#	1->Leer string de teclado
#	2->Primer Letra De Palabra Mayúscula
#	3->Primer letra de frase mayúscula
#	4->MAYÚSCULA TODO
#	5->minúscula todo


#Sección de Datos
.data
msjError:	.asciiz "Seleccion erronea, vuelva a seleccionar:\n"
noMsj	:	.asciiz "Funcionalidad en espera de implementacion\n"
msjMenu:	.ascii "Menu de Seleccion:\n"
		.ascii "1 => Leer string de teclado, MAX de 256 carateres\n "
		.ascii "2 => Primer Letra De Palabra Mayuscula\n "
		.ascii "3 => Primer letra de frase mayuscula\n "
		.ascii "4 => MAYUSCULA TODO\n "
		.asciiz "5 => minuscula todo\n "
msjInicio: 	.asciiz	"Ingrese una frase y digite Enter\n"
Entrada:	.asciiz "String de prueba, el CAFE es delicioso\n"

#Sección de Código
.text
main:
	#Cargar valores del menu de selección
	addi $s1,$0,1		#cargar 1 en t1
	addi $s2,$0,2		#cargar 2 en t2
	addi $s3,$0,3		#cargar 3 en t3
	addi $s4,$0,4		#cargar 4 en t4
	addi $s5,$0,5		#cargar 5 en t5
	#registros para hacer comparaciones		
	addi $s6,$0,0x7A	#z=0x7A
	addi $s7,$0,0x20	#[espacio]=0x20
	addi $s0,$0,0x0A        #\n=0x0A
menu:
#	add $t0,$0,$0		#t0=i=0=contador 
	la $a0, msjMenu      	#cargar msjInicio en $a0
	jal print_msj    	#imprime mensaje de Menu

	li $v0, 5	 	#leer selección de menu
	syscall
	addu $a0,$v0,$zero	#$a0 = selección de menu 

	beq $a0,$s1,case1	#saltar a menu 1	
	beq $a0,$s2,case2	#saltar a menu 2
	beq $a0,$s3,case3	#saltar a menu 3
	beq $a0,$s4,case4	#saltar a menu 4
	beq $a0,$s5,case5	#saltar a menu 5

	la $a0, msjError	#Selección errónea
	jal print_msj
	j menu

case1:			#Leer de teclado
	la $a0, msjInicio	#Selección errónea
	jal print_msj
	la $a0, Entrada 
	li $a1, 256
	li $v0, 8		#$a0=buffer
	syscall
	j menu 

case2:			#Cambiar letras basado en selección 2
	la $a0, Entrada
	jal  pldpm
	jal print_msj
	j menu

case3:			#Cambiar letras basado en selección 3
	la $a0, Entrada
	jal  pldfm
	jal print_msj
	j menu			

case4:			#Cambiar a MAYÚSCULAS
	la $a0, Entrada
	jal  mayus
	jal print_msj
	j menu			

case5:			#Cambiar a minúsculas
	la $a0, Entrada
	jal minus
	jal print_msj
	j menu


#Sección de Funciones
#Función para imprimir mensajes en pantalla
print_msj:
	li $v0, 4	       	#cargar print string a syscall
	syscall			#imprimir en pantalla msjinicio
	jr $ra

#Función para pldpm (Primer Letra De Palabra Mayúscula)
pldpm	:		
	add $sp,$sp,-8		#reservo pila para $a0  y $r0
	sw $a0,4($sp)		#a0 actual en memoria
	sw $ra,0($sp)		#a0 actual en memoria
	
	loop_pldpm:
	lbu $t1,0($a0)
	beq $t1,$s0,endloop_pldpm

	#Verificar si primer letra es mayus
	sltiu $t2,$t1,'A'	     #t2=1 si $t1 < A o 0x41 
	bne $t2,$0,no_letra	     #si t2=1 imprimir carácter sin modificación 
	addi $t3,$s6,-32             #'z'-32='Z' o 0x5A
	sltu $t2,$t3,$t1	     #t2=1 si $t1 > 0x5A o Z
	bne $t2,$0,es_pal_minus	     #si t=0 verificar si es minúscula	
	addi $a0,$a0,1		     #Pasar a siguiente carácter
        jal pal_minus
	j loop_pldpm		

	#Verificar si primer caracter es letra minúscula
	es_pal_minus:
	sltiu $t2,$t1,'a'	    #t2=1 si $t1 < 0x61 o 'a' 
	bne $t2,$0,no_letra  #si t2=1 imprimir carácter sin modificación 
	sltu $t2,$s6,$t1	    #t2=1 si $t1 > 0x7A o 'z'
	bne $t2,$0,no_letra  #si t2=1 imprimir carácter sin modificación
	andi $t1,$t1,0xdf	    #Cambiar carácter de minúscula a mayúscula
	sb $t1,0($a0)		#guardar cambio 
	addi $a0,$a0,1		#Pasar a sig.carácter
 	jal pal_minus 
	j loop_pldpm

	no_letra:
	addi $a0,$a0,1		#Pasar a siguiente carácter
	j loop_pldpm

	endloop_pldpm:
	lw $ra,0($sp)
	lw $a0,4($sp)
	add $sp,$sp,8		#liberar espacio reservado en  la pila
	jr $ra



#Función para pldfm (Primer letra de frase mayúscula)
pldfm:			#Función para pldfm		#
	add $sp,$sp,-8		#reservo pila para $a0  y $r0
	sw $a0,4($sp)		#a0 actual en memoria
	sw $ra,0($sp)		#a0 actual en memoria
	
	loop_pldfm:
	lbu $t1,0($a0)
	beq $t1,$s0,endloop_pldfm

	#Verificar si primer letra es mayus
	sltiu $t2,$t1,'A'	     #t2=1 si $t1 < A o 0x41 
	bne $t2,$0,no_es_letra	     #si t2=1 imprimir carácter sin modificación 
	addi $t3,$s6,-32             #'z'-32='Z' o 0x5A
	sltu $t2,$t3,$t1	     #t2=1 si $t1 > 0x5A 
	bne $t2,$0,es_minus	#si t=0 verificar si es minúscula	
	addi $a0,$a0,1		#Pasar a siguiente carácter
	jal minus		#pasar a cambiar resto de frase a minúscula
	j endloop_pldfm
	
	#Verificar si primer caracter es letra minúscula
	es_minus:
	sltiu $t2,$t1,'a'	    #t2=1 si $t1 < 0x61 o 'a' 
	bne $t2,$0,no_es_letra  #si t2=1 imprimir carácter sin modificación 
	sltu $t2,$s6,$t1	    #t2=1 si $t1 > 0x7A o 'z'
	bne $t2,$0,no_es_letra  #si t2=1 imprimir carácter sin modificación
	andi $t1,$t1,0xdf	    #Cambiar carácter de minúscula a mayúscula
	sb $t1,0($a0)		#guardar cambio 
	addi $a0,$a0,1		#Pasar a siguiente carácter y llamar a minus 
	jal minus
	j endloop_pldfm

	no_es_letra:
	addi $a0,$a0,1		#Pasar a siguiente carácter
	j loop_pldfm

	endloop_pldfm:
	lw $ra,0($sp)
	lw $a0,4($sp)
	add $sp,$sp,8		#liberar espacio reservado en  la pila
	jr $ra

#Funcion para convertir string a mayúsculas
mayus:
	add $sp,$sp,-8		#reservo pila para $a0  y $r0
	sw $a0,4($sp)		#a0 actual en memoria
	sw $ra,0($sp)		#a0 actual en memoria

	loop_mayus:
	lbu $t1,0($a0)
	beq $t1,$s0,endloop_mayus
	add $t6,$0,$s6
	sltiu $t2,$t1,'a'	    #t2=1 si $t1 < 0x61 o 'a' 
	bne $t2,$0,no_cambia_mayus  #si t2=1 imprimir carácter sin modificación 
	sltu $t2,$s6,$t1	    #t2=1 si $t1 > 0x7A o 'z'
	bne $t2,$0,no_cambia_mayus  #si t2=1 imprimir carácter sin modificación
	andi $t1,$t1,0xdf	    #Cambiar carácter de minúscula a mayúscula
	sb $t1,0($a0)		#guardar cambio 
	addi $a0,$a0,1		#Pasar a siguiente carácter
	j loop_mayus
	
	no_cambia_mayus:	#Si carácter actual debe mantenerse igual
	add $a0,$a0,1			#aumentar un carácter en entrada
	j loop_mayus

	endloop_mayus:
	lw $ra,0($sp)
	lw $a0,4($sp)
	add $sp,$sp,8		#liberar espacio reservado en  la pila
	jr $ra
#Funcion para convertir frase a minúsculas
minus:
	add $sp,$sp,-8		#reservo pila para $a0  y $r0
	sw $a0,4($sp)		#a0 actual en memoria
	sw $ra,0($sp)		#r0 actual en memoria
		#lw $t1,0($a0)		#
	loop_minus:
	lbu $t1,0($a0)
	beq $t1,$s0,endloop_minus
	sltiu $t2,$t1,'A'	     #t2=1 si $t1 < A o 0x41 
	bne $t2,$0,no_cambia_minus   #si t2=1 imprimir carácter sin modificación 
	addi $t3,$s6,-32             #'z'-32='Z' o 0x5A
	sltu $t2,$t3,$t1	     #t2=1 si $t1 > 0x5A o 'z'
	bne $t2,$0,no_cambia_minus   #si t=1 imprimir carácter sin modificación	
	ori $t1,$t1,0x20        #Cambiar carácter de mayúscula a minúscula
	sb $t1,0($a0)		#guardar cambio 
	addi $a0,$a0,1		#Pasar a siguiente carácter
	j loop_minus

	no_cambia_minus:		#Si carácter actual debe mantenerse igual
	add $a0,$a0,1			#aumentar un carácter en entrada
	j loop_minus
	
	endloop_minus:
	lw $ra,0($sp)
	lw $a0,4($sp)
	add $sp,$sp,8		#liberar espacio reservado en  la pila
	jr $ra
	

pal_minus:		#convertir palabra a minúsculas

	add $sp,$sp,-4		#reservo pila para $r0
	sw $ra,0($sp)		#r0 actual en memoria
			#Aquí no guardo a0 en pila ya que al estar cambiando  
			#una palabra de una frase cuando encuentro un espacio
			#quiero retornar al punto justo después del espacio  
			#dónde se encuentra la sig. palabra a evaluar
	loop_pal_minus:
	lbu $t1,0($a0)
	beq $t1,$s0,endloop_pal_minus
	beq $t1,$s7,endloop_pal_minus
	sltiu $t2,$t1,'A'	     #t2=1 si $t1 < A o 0x41 
	bne $t2,$0,no_cambia_pal_minus  #si t2=1 imprimir carácter sin modificación 
	addi $t3,$s6,-32             #'z'-32='Z' o 0x5A
	sltu $t2,$t3,$t1	     #t2=1 si $t1 > 0x5A o 'Z'
	bne $t2,$0,no_cambia_pal_minus  #si t2=1 imprimir carácter sin modificar
	ori $t1,$t1,0x20        #Cambiar carácter de mayúscula a minúscula
	sb $t1,0($a0)		#guardar cambio 
	addi $a0,$a0,1		#Pasar a siguiente carácter
	j loop_pal_minus

	no_cambia_pal_minus:	#Si carácter actual debe mantenerse igual
	add $a0,$a0,1		#aumentar un carácter en entrada
	j loop_pal_minus
	
	endloop_pal_minus:
	lw $ra,0($sp)
	add $sp,$sp,4		#liberar espacio reservado en  la pila
	jr $ra
#Funcion para imprimir mensaje de no implemantada, en caso de ser necesario
not_implemented:
	la $a0, noMsj
	li $v0, 4	       	#cargar print string a syscall
	syscall			#imprimir en pantalla msjinicio
	jr $ra
	
